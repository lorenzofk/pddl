(define (problem pb3)
  (:domain hp)
  ;(:requirements :strips :typing :negative-preconditions)
  (:objects
    a1 - location
    a2 - location
    a3 - location
    a4 - location

    b1 - location
    b2 - location
    b3 - location
    b4 - location

    c1 - location
    c2 - location
    c3 - location
    c4 - location

    d1 - location
    d2 - location
    d3 - location
    d4 - location

    g - house
    h - house
    r - house
    s - house
  )

  (:init
    (gryffindor g)
    (hufflepuff h)
    (ravenclaw r)
    (slytherin s)

    (not(tile a1))
    (tile a2)
    (tile a3)
    (tile a4)
    (tile b1)
    (tile b2)
    (tile b3)
    (tile b4)
    (tile c1)
    (tile c2)
    (tile c3)
    (tile c4)
    (tile d1)
    (tile d2)
    (tile d3)
    (tile d4)

    (line a2 a3 a4)

    (line b1 b2 b3)
    (line b2 b3 b4)

    (line c1 c2 c3)
    (line c2 c3 c4)

    (line d1 d2 d3)
    (line d2 d3 d4)

    (line b1 c1 d1)

    (line a2 b2 c2)
    (line b2 c2 d2)

    (line a3 b3 c3)
    (line b3 c3 d3)

    (line a4 b4 c4)
    (line b4 c4 d4)

    (position a2 s)
    (position a3 g)
    (position a4 h)

    (position b1 s)
    (position b2 h)
    (position b3 g)
    (position b4 r)

    (position c1 s)
    (position c2 h)
    (position c3 r)
    (position c4 g)

    (position d1 h)
    (position d2 h)
    (position d3 h)
    (position d4 r)

    (adj a2 a3)
    (adj a2 b2)

    (adj a3 a2)
    (adj a3 b3)
    (adj a3 a4)

    (adj a4 a3)
    (adj a4 b4)

    (adj b1 b2)
    (adj b1 c1)

    (adj b2 b1)
    (adj b2 a2)
    (adj b2 b3)

    (adj b3 b2)
    (adj b3 a3)
    (adj b3 b4)

    (adj b4 b3)
    (adj b4 a4)
    (adj b4 c4)

    (adj c1 b1)
    (adj c1 c2)

    (adj c2 c1)
    (adj c2 b2)
    (adj c2 c3)

    (adj c3 c2)
    (adj c3 b3)
    (adj c3 c4)

    (adj c4 c3)
    (adj c4 b4)
    (adj c4 d4)

    (adj d1 d2)
    (adj d1 c1)

    (adj d2 d1)
    (adj d2 c2)
    (adj d2 d3)

    (adj d3 d2)
    (adj d3 c3)
    (adj d3 d4)

    (adj d4 d3)
    (adj d4 c4)

  )

  (:goal
    (and
      (not(tile a1))
      (not(tile a2))
      (not(tile a3))
      (not(tile a4))
      (not(tile b1))
      (not(tile b2))
      (not(tile b3))
      (not(tile b4))
      (not(tile c1))
      (not(tile c2))
      (not(tile c3))
      (not(tile c4))
      (not(tile d1))
      (not(tile d2))
      (not(tile d3))
      (not(tile d4))
    )
  )
)
