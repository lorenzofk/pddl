(define (domain hp)
  (:requirements
    :strips
    :typing
    :negative-preconditions
  )

  (:types
    location
    house
  )

  (:predicates
    (tile ?x - location)
    (adj ?s1 ?s2 - location)
    (position ?p1 - location ?house - house)
    (gryffindor ?k1 - house)
    (hufflepuff ?k1 - house)
    (ravenclaw ?k1 - house)
    (slytherin ?k1 - house)
    (line ?s1 ?s2 ?s3)
  )

  (:action swap
    :parameters (?from ?to - location ?houseFrom ?houseTo - house)
    :precondition
      (and
        (tile ?from)
        (tile ?to)
        (adj ?from ?to) ; they are neighbours
        (position ?from ?houseFrom)
        (position ?to ?houseTo)
      )
    :effect
      (and
        (not (position ?from ?houseFrom))
        (not (position ?to ?houseTo))
        (position ?from ?houseTo)
        (position ?to ?houseFrom)
      )
  )

  (:action match
    :parameters (?l1 ?l2 ?l3 - location ?house - house)
    :precondition
    (and
      (line ?l1 ?l2 ?l3)
      (position ?l1 ?house)
      (position ?l2 ?house)
      (position ?l3 ?house)
      (adj ?l1 ?l2)
      (adj ?l2 ?l3)
    )
    :effect
    (and
      (not (tile ?l1))
      (not (tile ?l2))
      (not (tile ?l3))
    )
  )
)
