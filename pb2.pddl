(define (problem pb2)
  (:domain hp)
  ;(:requirements :strips :typing :negative-preconditions)
  (:objects
    a1 - location
    a2 - location
    a3 - location
    b1 - location
    b2 - location
    b3 - location
    c1 - location
    c2 - location
    c3 - location
    d1 - location
    d2 - location
    d3 - location

    g - house
    h - house
    r - house
    s - house
  )

  (:init
    (gryffindor g)
    (hufflepuff h)
    (ravenclaw r)
    (slytherin s)

    (tile a1)
    (tile a2)
    (tile a3)
    (tile b1)
    (tile b2)
    (tile b3)
    (tile c1)
    (tile c2)
    (tile c3)
    (tile d1)
    (tile d2)
    (tile d3)

    (line a1 a2 a3)
    (line b1 b2 b3)
    (line c1 c2 c3)
    (line d1 d2 d3)

    (line a1 b1 c1)
    (line b1 c1 d1)

    (line a2 b2 c2)
    (line b2 c2 d2)

    (line a3 b3 c3)
    (line b3 c3 d3)

    (position a1 g)
    (position a2 h)
    (position a3 g)

    (position b1 h)
    (position b2 h)
    (position b3 s)

    (position c1 s)
    (position c2 s)
    (position c3 g)

    (position d1 r)
    (position d2 r)
    (position d3 r)

    (adj a1 a2)
    (adj a1 b1)

    (adj a2 a1)
    (adj a2 a3)
    (adj a2 b2)

    (adj a3 a2)
    (adj a3 b3)

    (adj b1 a1)
    (adj b1 b2)
    (adj b1 c1)

    (adj b2 b1)
    (adj b2 a2)
    (adj b2 b3)

    (adj b3 b2)
    (adj b3 a3)

    (adj c1 b1)
    (adj c1 c2)

    (adj c2 c1)
    (adj c2 b2)
    (adj c2 c3)

    (adj c3 c2)
    (adj c3 b3)

    (adj d1 d2)
    (adj d1 c1)

    (adj d2 d1)
    (adj d2 c2)
    (adj d2 d3)

    (adj d3 d2)
    (adj d3 c3)

  )

  (:goal
    (and
      (not(tile a1))
      (not(tile a2))
      (not(tile a3))
      (not(tile b1))
      (not(tile b2))
      (not(tile b3))
      (not(tile c1))
      (not(tile c2))
      (not(tile c3))
      (not(tile d1))
      (not(tile d2))
      (not(tile d3))
    )
  )
)
