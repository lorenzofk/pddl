(define (problem pb2)
  (:domain hp)
  ;(:requirements :strips :typing :negative-preconditions)
  (:objects
    a1 - location
    a2 - location
    a3 - location
    b1 - location
    b2 - location
    b3 - location
    c1 - location
    c2 - location
    c3 - location

    g - house
    h - house
    r - house
  )

  (:init
    (gryffindor g)
    (hufflepuff h)
    (ravenclaw r)

    (tile a1)
    (tile a2)
    (tile a3)
    (tile b1)
    (tile b2)
    (tile b3)
    (tile c1)
    (tile c2)
    (tile c3)

    (line a1 a2 a3)
    (line b1 b2 b3)
    (line c1 c2 c3)

    (line a1 b1 c1)
    (line a2 b2 c2)
    (line a3 b3 c3)

    (position a1 g)
    (position a2 h)
    (position a3 g)

    (position b1 h)
    (position b2 h)
    (position b3 r)

    (position c1 r)
    (position c2 r)
    (position c3 g)

    (adj a1 a2)
    (adj a2 a1)

    (adj a2 a3)
    (adj a3 a2)

    (adj a1 b1)
    (adj b1 a1)

    (adj b1 b2)
    (adj b2 b1)

    (adj b2 a2)

    (adj b2 b3)
    (adj b3 b2)

    (adj b3 a3)
    (adj a3 b3)

    (adj c1 b1)
    (adj b1 c1)

    (adj c1 c2)
    (adj c2 c1)

    (adj c2 b2)
    (adj b2 c2)

    (adj c2 c3)
    (adj c3 c2)

    (adj c3 b3)
    (adj b3 c3)


  )

  (:goal
    (and
      (not(tile a1))
      (not(tile a2))
      (not(tile a3))
      (not(tile b1))
      (not(tile b2))
      (not(tile b3))
      (not(tile c1))
      (not(tile c2))
      (not(tile c3))
    )
  )
)
